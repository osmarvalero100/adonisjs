'use strict'

const Project = use('App/Models/Project');
const AuthorizationService = use('App/Services/AuthorizationService');

class ProjectController {

  async index ({auth}) {
    const user = await auth.getUser();
    return user.projects().fetch();
  }

  async store ({auth, request}) {
    const user = await auth.getUser();
    const {name} = request.all();
    const project = new Project();
    project.fill({
      name
    });
    await user.projects().save(project)
    return project;
  }

  async update ({auth, request, params}) {
    const user = await auth.getUser();
    const {id} = params;
    const project = await Project.find(id);
    AuthorizationService.verifyPermition(project, user);
    project.merge(request.only('name'));
    await project.save();
    return project;
  }

  async destroy ({auth, response, params}) {
    const user = await auth.getUser();
    const {id} = params;
    // exception validate type UUID
    const project = await Project.find(id);
    if (!project) {
      return response
        .status(404)
        .send({ message: { error: 'Not found registered' } })
    }
    AuthorizationService.verifyPermition(project, user);
    
    await project.delete();
    return project;
  }
}

module.exports = ProjectController
