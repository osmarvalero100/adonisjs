'use strict'

const AuthorizationService = use('App/Services/AuthorizationService');
const Project = use('App/Models/Project');
const Task = use('App/Models/Task');

class TaskController {
  async index ({auth, request, params}) {
    const user = await auth.getUser();
    const {id} = params;
    const project = await Project.find(id);
    AuthorizationService.verifyPermition(project, user);
    return await project.tasks().fetch();
  }

  async store ({auth, request, params}) {
    const user = await auth.getUser();
    const {description} = request.all();
    const {id} = params;
    const project = await Project.find(id);
    AuthorizationService.verifyPermition(project, user);
    const tarea = new Task();
    tarea.fill({
      description
    });
    await project.tasks().save(tarea);
    return tarea;
  }

  async update ({auth, request, response, params}) {
    const user = await auth.getUser();
    const {id} = params;
    const task = await Task.find(id);
    // exception validate type UUID
    const project = await task.project().fetch();
    if (!project) {
      return response
        .status(404)
        .send({ message: { error: 'Not found registered' } })
    }
    AuthorizationService.verifyPermition(project, user);
    task.merge(request.only([
      'description',
      'success'
    ]));
    
    await task.save();
    return task;
  }

  async destroy ({auth, response, params}) {
    const user = await auth.getUser();
    const {id} = params;
    const task = await Task.find(id);
    // exception validate type UUID
    const project = await task.project().fetch();
    if (!project) {
      return response
        .status(404)
        .send({ message: { error: 'Not found registered' } })
    }
    AuthorizationService.verifyPermition(project, user);
    
    await task.delete();
    return task;
  }
}

module.exports = TaskController
