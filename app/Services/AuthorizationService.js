const AccessDeniedException = use('App/Exceptions/AccessDeniedException');

class AuthorizationService {
  verifyPermition(resource, user) {
    if (resource.user_id !== user.id) {
      throw new AccessDeniedException();
    }
  }
}

module.exports = new AuthorizationService();