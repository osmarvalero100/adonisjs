# Adonis API application

This is the boilerplate for creating an API server in AdonisJs, it comes pre-configured with.

1. Bodyparser
2. Authentication
3. CORS
4. Lucid ORM
5. Migrations and seeds

## Setup

Use the adonis command to install the blueprint

```bash
adonis new yardstick --api-only
```

or manually clone the repo and then run `npm install`.


### Migrations

Run the following command to run startup migrations.

```js
adonis migration:run
```
# API
## Endpoints
1. /api/v1/users/register : Registro de usuarios
    - method: POST
    - params: 
        - email: type string
        - password: type string
    - return: JWT
2. /api/v1/users/login : Obtener JWT
    - method: POST
    - params: 
        - email: type string
        - password: type string
    - return: JWT
3. /api/v1/projects : Crea un proyecto y los asocia a un usuario
    - method: POST
    - params: 
        - name: string
    - headers:
        - Authorization: Bearer JWT
    - return: json
4. /api/v1/projects : Obtiene los proyectos de un usuario
    - method: GET
    - headers:
        - Authorization: Bearer JWT
    - return: json


