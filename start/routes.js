'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URLs and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.get('/', () => {
  return {
    'api': 'v1/',
    'url': 'https://www.youtube.com/watch?v=gORRR84ICzo&list=PLs-v5LWbw7JkPXd3q_F-18S0cGy-Z2o8l&index=12'
  };
});

Route.group(() => {
  Route.post('/users/register', 'UserController.store');
  Route.post('/users/login', 'UserController.login');
  Route.get('/projects', 'ProjectController.index').middleware('auth');
  Route.post('/projects', 'ProjectController.store').middleware('auth');
  Route.delete('/projects/:id', 'ProjectController.destroy').middleware('auth');
  Route.get('/projects/:id/tasks', 'TaskController.index').middleware('auth');
  Route.post('/projects/:id/tasks', 'TaskController.store').middleware('auth');
  Route.patch('/tasks/:id', 'TaskController.update').middleware('auth');
  Route.delete('/tasks/:id', 'TaskController.destroy').middleware('auth');
}).prefix('api/v1');


