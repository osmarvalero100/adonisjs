'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class TaskSchema extends Schema {
  up () {
    this.create('tasks', (table) => {
      table.uuid('id').unique().defaultTo(this.db.raw('public.gen_random_uuid()'))
      table.uuid('project_id').references('id').inTable('projects')
      table.string('description', 250).notNullable()
      table.boolean('success').defaultTo(false)
      table.timestamps()
    })
  }

  down () {
    this.drop('tasks')
  }
}

module.exports = TaskSchema
